import mongoengine as db

class Message(db.Document):
    body = db.StringField(max_length=255, min_length=1, required=True)
    sender = db.StringField(max_length=55)
    receiver = db.StringField(max_length=55)
    tags = db.ListField(db.StringField(max_length=50))
    # meta = { 'allow_inheritance' : False }