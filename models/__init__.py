from mongoengine import *
from configuration import database

connect(
    db=database['database'],
    host=database['host'],
    port=database['port']
)