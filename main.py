from flask import Flask

app = Flask(__name__)

app.config['SECRET_KEY'] = 'ayy_limao'

import routes.home_routes as home_routes
import routes.message_routes as message_routes
app.register_blueprint(home_routes.home_routes)
app.register_blueprint(message_routes.message_route)

if __name__ == '__main__':
    app.run()