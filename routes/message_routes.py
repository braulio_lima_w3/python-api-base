from flask import request, Blueprint, jsonify, abort
import services.message_service as service
from helpers.req_helpers import as_json, json_of
from helpers.catch_all import has_secret_key

message_route = Blueprint('message', __name__, url_prefix='/messages')

@message_route.route('/', methods=['POST', 'GET'])
@has_secret_key
def save_or_get_all():
    if request.method == 'GET':
        return as_json(service.get_all().to_json())
    elif request.method == 'POST':
        return as_json(service.save( **json_of() ).to_json())
    else:
        abort(404)

@message_route.route('/<id>', methods=['PUT', 'GET', 'DELETE'])
@has_secret_key
def update_del_get(id):
    if not id:
        abort(400)
    if request.method == 'PUT':
        return as_json(service.save( **json_of(), id = id ).to_json())
    elif request.method == 'DELETE':
        service.delete(id)
        return jsonify(True)
    elif request.method == 'GET':
        return as_json(service.get(id).to_json())
    abort(404)

