from flask import Blueprint

home_routes = Blueprint('default', __name__)

@home_routes.route('/')
def home():
    return 'hello'