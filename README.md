# Intro

**THIS IS A DEMO PROJECT USED TO SHOWCASE SOME OF FLASK'S RESTFUL CAPABILITIES**

# First time config

## Install dependencies

Assuming the developer is using **`python 3+`**

Execute one or the other:

    python -m pip install -r requirements.txt

or

    pip -r requirements.txt

# Running

**Note:** Make sure mongodb is running.

Execute one of the following:

    python main.py

or

    FLASK_APP=main python -m flask run

Just a note on the above command. Make sure `FLASK_APP` correspond to the name of the python file.

# Other information

Make sure you have mongo running in the background in order to have access to the database.