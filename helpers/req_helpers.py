from flask import request, Response
from flask.helpers import Headers

def json_of():
    try:
        output = request.get_json(force=True)
        return output if output else {}
    except Exception as e:
        print(e)
    return {}

def as_json(string_val='', status=200):
    headers = Headers()
    headers.add_header('Content-Type', 'application/json; charset=utf-8')
    resp = Response(string_val, status, headers)
    return resp