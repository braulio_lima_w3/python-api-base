from flask import url_for, redirect, request, jsonify
from functools import wraps

# def has_secret_key(fun):
#     @wraps(fun)
#     def decorated(*args, **kwargs):
#         head = request.headers if request.headers else {}
#         if 'abra kadabra' != head.get('Authorization'):
#             return jsonify('bye'), 403
#         return fun(*args, **kwargs)
#     return decorated

def has_secret_key(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        head = request.headers if request.headers else {}
        if 'abra kadabra' != head.get('Authorization'):
            return jsonify('bye'), 403
        return f(*args, **kwargs)
    return decorated_function
