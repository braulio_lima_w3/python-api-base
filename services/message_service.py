from models.message import Message
from exception import AppException
import bson

def save(body='', sender='', receiver='', tags=[], id=''):
    msg = Message()
    if id:
        msg.id = bson.ObjectId(id)
    msg.body = body
    msg.sender = sender
    msg.receiver = receiver
    msg.tags = tags if tags else []
    return msg.save()

def get(id=''):
    if not id:
        return None
    return Message.objects.get(id=id)

def addTags(id='', tags=[]):
    found = get(id)
    if not found:
        return
    found.tags = (found.tags if found.tags else []) + tags
    return found.save()

def delete(id=''):
    output = get(id)
    if not output:
        return None
    output.delete()
    return output

def get_all():
    output = Message.objects()
    return output if output else []