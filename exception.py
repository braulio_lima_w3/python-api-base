class AppException(Exception):
    def __init__(self, message):
        super().__init__(message)
    def set_field(self, field):
        self.field = field
        return self
    def set_status(self, status):
        self.status = status
        return self